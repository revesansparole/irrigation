"""
Library of components for this set of TPs
"""
from datetime import datetime, timedelta
import numpy as np


def doy(date):
    return date.timetuple().tm_yday


def date(year, doy):
    return datetime(year, 1, 1) + timedelta(days=doy - 1)


def rmse(sim, obs):
    """Root mean squared error (RMSE).

    Args:
        sim (np.array): [-] simulation results
        obs (np.array): [-] observation along the same axis

    Returns:
        (float)
    """
    return np.sqrt(np.sum(np.subtract(obs, sim) ** 2) / len(obs))


def rrmse(sim, obs):
    """Relative root mean squared error

    Notes: this function does not handle 'NaN'

    Args:
        sim (np.array): [-] simulation results
        obs (np.array): [-] observation along the same axis

    Returns:
        (float)
    """
    return rmse(sim, obs) / abs(np.mean(obs))


def vg_psi(theta, theta_res, theta_sat, alpha_inv, n_inv):
    """Van Genuchten formalism to compute matric potential

    References:
        - Van Genuchten (1980)

    Args:
        theta (float): [m3.m-3] soil moisture
        theta_res (float): [m3.m-3] residual soil moisture
        theta_sat (float): [m3.m-3] soil moisture at saturation
        alpha_inv (float): [MPa] scaling parameter
        n_inv (float): [-] shape parameter

    Returns:
        (float): [MPa] soil matric potential
    """
    m_inv = 1 / (1 - n_inv)

    sat = (theta - theta_res) / (theta_sat - theta_res)
    sat = min(max(1e-3, sat), 1.)

    h = alpha_inv * (1 / sat ** m_inv - 1) ** n_inv

    return -h


def vg_theta(psi, theta_res, theta_sat, alpha, n):
    """Van Genuchten formalism to compute soil moisture

    References:
        - Van Genuchten (1980)

    Args:
        psi (float): [MPa] matric potential
        theta_res (float): [m3.m-3] residual soil moisture
        theta_sat (float): [m3.m-3] soil moisture at saturation
        alpha (float): [MPa-1] scaling parameter
        n (float): [-] shape parameter

    Returns:
        (float): [m3.m-3] soil moisture
    """
    m = 1 - 1 / n

    sat = (1 / (1 + (alpha * -psi) ** n)) ** m

    return theta_res + sat * (theta_sat - theta_res)
