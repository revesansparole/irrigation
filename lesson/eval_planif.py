"""
Small wrapper around spa package to run the simulation and plot indicators
"""
from dataclasses import dataclass
from datetime import datetime, timedelta
from pathlib import Path

import pandas as pd
from agrosim.io import read_json
from agrosim.params import ParamsBase
from agrosim.process import MemoryProcess, Process, one_hour
from plotly.subplots import make_subplots
from spa.params_management import instantiate_params, soil_from_texture
from spa.simu.itk import ItkProcess
from spa.simu.model import Model
from spa.simu.simulator import Simulator


@dataclass
class FieldParams(ParamsBase):
    """Parameters related to the simulated field
    """

    latitude: float = 43
    """[°] Latitude of field (positive North)
    """

    area: float = 1e4
    """[m2] Surface of field
    """

    density: float = 1 / 0.8 ** 2
    """[plant.m-2] Density of plantation
    """

    texture: str = 'silty clay loam'
    """[-] Name of soil texture
    """


class ProcessSubSurface(Process):
    def update(self, t, model, params):
        model.soil.irrig[0], model.soil.irrig[1] = model.soil.irrig[1], model.soil.irrig[0]

        return t + one_hour


class ProcessOutput(MemoryProcess):
    def update(self, t, model, params):
        entry = dict(
            date=t,
            drainage=model.soil.drainage,
            evap=model.soil.evap,
            irrig=sum(model.soil.irrig),
            runoff=model.soil.runoff,
            uptake=model.soil.uptake,
            stress=model.plant.potential,
        )

        self.append(entry)

        return t + one_hour


def accident(typ):
    doy_ini = 220
    if typ == 'leak':
        scaling = 0.5
        doy_fin = 240
    elif typ == 'overflow':
        scaling = 1.3
        doy_fin = 240
    elif typ == 'block':
        scaling = 0.
        doy_fin = 230
    else:
        raise NotImplementedError(f"accident type unknown '{typ}', must be in ('leak', 'overflow')")

    def deco(planif):
        def wrapper(t, forecast):
            irrigs = planif(t, forecast)

            irrigs_acc = []
            for row, irrig in zip(forecast, irrigs):
                if doy_ini <= row['date'].timetuple().tm_yday <= doy_fin:
                    irrig *= scaling
                irrigs_acc.append(irrig)

            return irrigs_acc

        return wrapper

    return deco


def simu_params(field):
    params = instantiate_params(read_json(Path(__file__).parent / "data/inputs_ref.json"))
    params.field.latitude = field.latitude
    params.field.area = field.area
    params.field.density = field.density
    soil_from_texture(params, field.texture)

    params.state.thetas = [vox.theta_fc for vox in params.soil.voxels]

    return params


def display_evaluation(hdf, params, display):
    if "season" in display:
        # restrict plot to plant growing period
        doy0 = datetime(params.weather[0].date.year, 1, 1)
        t_ini = doy0 + timedelta(days=params.plant.plantation - 1)
        t_fin = doy0 + timedelta(days=params.plant.senescence - 1)

        hdf = hdf.loc[t_ini:t_fin]

    if "no" in display:
        return hdf
    else:
        # plot result
        fig = make_subplots(rows=2, cols=1, shared_xaxes=True, vertical_spacing=0.01)
        coords = dict(row=1, col=1)
        fig.add_scatter(x=hdf.index, y=hdf['stress'], mode='lines', name="stress",
                        showlegend=True, **coords)
        fig.add_hrect(y0=-0.1, y1=-0.04, line_width=0, fillcolor="green", opacity=0.2)
        fig.update_yaxes(title_text="[MPa]", **coords)

        coords = dict(row=2, col=1)
        fig.add_scatter(x=hdf.index, y=hdf['uptake'].cumsum(), mode='lines', line=dict(width=3),
                        name="uptake", showlegend=True, **coords)
        fig.add_scatter(x=hdf.index, y=hdf['irrig'].cumsum(), mode='lines', line=dict(width=3),
                        name="irrig", showlegend=True, **coords)
        fig.add_scatter(x=hdf.index, y=hdf['evap'].cumsum(), mode='lines', line=dict(dash='dash'),
                        name="evaporation", showlegend=True, **coords)
        fig.add_scatter(x=hdf.index, y=hdf['runoff'].cumsum(), mode='lines', line=dict(dash='dash'),
                        name="runoff", showlegend=True, **coords)
        fig.add_scatter(x=hdf.index, y=hdf['drainage'].cumsum(), mode='lines', line=dict(dash='dash'),
                        name="drainage", showlegend=True, **coords)
        fig.update_yaxes(title_text="[m3]", **coords)
        return fig


def eval_planif(field, planif, display="year"):
    params = simu_params(field)
    params.planif = planif

    # perform computation
    simu = Simulator(Model(params), params)
    pout = ProcessOutput()
    simu.add_process(pout)

    simu.run()

    hdf = pd.DataFrame(pout.retrieve()).set_index('date')
    return display_evaluation(hdf, params, display)


def eval_subsurface(field, planif, display="year"):
    params = simu_params(field)
    params.planif = planif

    # perform computation
    simu = Simulator(Model(params), params)
    simu.add_process(ProcessSubSurface(), after=ItkProcess)
    pout = ProcessOutput()
    simu.add_process(pout)

    simu.run()

    hdf = pd.DataFrame(pout.retrieve()).set_index('date')
    return display_evaluation(hdf, params, display)


def eval_accident(typ, field, planif, display="year"):
    return eval_planif(field, accident(typ)(planif), display)


if __name__ == "__main__":
    myfield = FieldParams()


    def planif_nil(t, forecast):
        return [0e-3 * myfield.area] * 7


    myparams = instantiate_params({})


    def planif_eto(t, forecast):
        doy = t.timetuple().tm_yday

        if doy < myparams.plant.plantation or doy > myparams.plant.senescence:
            return [0.] * 7

        if doy < myparams.plant.maturity:
            transpi = 5 / 7  # [L.day-1]
        else:
            transpi = 15 / 7  # [L.day-1]

        plant_needs = [transpi * 1e-3 * myfield.density] * 7
        evap = 1500 / 138  # [m3.field-1]

        irrigs = [v * myfield.area + evap for v in plant_needs]
        print("week irrig", sum(irrigs))

        return irrigs


    eval_planif(myfield, planif_eto).show()
