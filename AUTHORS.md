# Credits

## Development Lead

[//]: # ({# pkglts, doc.authors)

* jerome, <jerome.chopard@itk.fr>

[//]: # (#})

## Contributors

[//]: # ({# pkglts, doc.contributors)
* jchopard <jerome.chopard@itk.fr>
* Jerome Chopard <revesansparole@gmail.com>

[//]: # (#})
